package com.fastdemo.sso.dao.user;

import com.fastdemo.sso.entity.user.User;
import java.util.List;

/**
 * @Description auto create by schema-tool-V1.0
 * 
 * @author guxingchun
 * @date 2018-09-04 17:30:24
 * 
 */
public interface UserMapper {
    int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int insertBatch(List<User> user);

    int deleteBatchById(List<String> ids);

    int deleteBySelective(User user);

    int updateBatchById(List<User> user);

    int updateByIds(List<String> ids, User user);

    int updateBySelective(User user);

    List<User> selectByIds();

    List<User> selectBySelective(User user);

    List<User> selectCountBySelective(User user);

    List<User> selectIdBySelective(User user);
}