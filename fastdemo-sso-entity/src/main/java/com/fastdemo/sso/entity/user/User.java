package com.fastdemo.sso.entity.user;

/**
 * @Description auto create by schema-tool-V1.0
 * 
 * @author guxingchun
 * @date 2018-09-04 17:30:24
 * 
 */
public class User {
    /**
     * COLUMN: id
     */
    private String id;

    /**
     * 用户名 COLUMN: name
     */
    private String name;

    /**
     * 密码 COLUMN: password
     */
    private String password;

    /**
     * 邮箱 COLUMN: email
     */
    private String email;

    /**
     * 邮箱认证：0=未认证；1=已认证 COLUMN: email_certified
     */
    private Integer emailCertified;

    /**
     * 手机 COLUMN: mobile
     */
    private String mobile;

    /**
     * 手机认证：0=未认证；1=已认证 COLUMN: mobile_certified
     */
    private Integer mobileCertified;

    /**
     * 固定号码 COLUMN: telephone
     */
    private String telephone;

    /**
     * 姓名 COLUMN: real_name
     */
    private String realName;

    /**
     * 性别:0女1男 COLUMN: sex
     */
    private Integer sex;

    /**
     * 身份证号 COLUMN: id_no
     */
    private String idNo;

    /**
     * 身份证图片地址 COLUMN: id_img_back_url
     */
    private String idImgBackUrl;

    /**
     * 身份证图片地址 COLUMN: id_img_front_url
     */
    private String idImgFrontUrl;

    /**
     * 1：开启；3：删除；2：关闭 COLUMN: state
     */
    private Integer state;

    /**
     * 秘钥 COLUMN: secret_key
     */
    private String secretKey;

    /**
     * 实名认证：0=未认证；1=已认证 COLUMN: id_certified
     */
    private Integer idCertified;

    /**
     * 用户经纬度以逗号分隔 COLUMN: location
     */
    private String location;

    /**
     * 用户类型 0个人 1企业 COLUMN: user_type
     */
    private Integer userType;

    /**
     * 户籍所在省(id) COLUMN: reg_prov
     */
    private String regProv;

    /**
     * 户籍所在市(id) COLUMN: reg_city
     */
    private String regCity;

    /**
     * 户籍所在县(id) COLUMN: reg_country
     */
    private String regCountry;

    /**
     * 户籍所在具体地址 COLUMN: reg_add
     */
    private String regAdd;

    /**
     * 居住所在省(id) COLUMN: place_prov
     */
    private String placeProv;

    /**
     * 居住所在市(id) COLUMN: place_city
     */
    private String placeCity;

    /**
     * 居住所在县(id) COLUMN: place_country
     */
    private String placeCountry;

    /**
     * 居住所在具体地址 COLUMN: place_address
     */
    private String placeAddress;

    /**
     * 户籍所在省(名字) COLUMN: reg_prov_name
     */
    private String regProvName;

    /**
     * 户籍所在市(名字) COLUMN: reg_city_name
     */
    private String regCityName;

    /**
     * 户籍所在县(名字) COLUMN: reg_country_name
     */
    private String regCountryName;

    /**
     * 居住所在省(名字) COLUMN: place_prov_name
     */
    private String placeProvName;

    /**
     * 居住所在市(名字) COLUMN: place_city_name
     */
    private String placeCityName;

    /**
     * 居住所在县(名字) COLUMN: place_country_name
     */
    private String placeCountryName;

    /**
     * 公司 COLUMN: company
     */
    private String company;

    /**
     * 公司code COLUMN: company_code
     */
    private String companyCode;

    /**
     * qq的openId COLUMN: qq_open_id
     */
    private String qqOpenId;

    /**
     * 微信的openId COLUMN: weixin_open_id
     */
    private String weixinOpenId;

    /**
     * 头像 COLUMN: head_img
     */
    private String headImg;

    /**
     * 部门id COLUMN: dept_id
     */
    private String deptId;

    /**
     * 部门名称 COLUMN: dept_name
     */
    private String deptName;

    /**
     * 签名 COLUMN: signature
     */
    private String signature;

    /**
     * 1-是 0-否 COLUMN: is_console
     */
    private Integer isConsole;

    /**
     * 后台登录标志；1：开启；3：删除；2：关闭 COLUMN: console_state
     */
    private Integer consoleState;

    /**
     * COLUMN: recommend_code
     */
    private String recommendCode;

    /**
     * COLUMN: recommend_name
     */
    private String recommendName;

    /**
     * 所在网点ID COLUMN: recommend_dept_id
     */
    private String recommendDeptId;

    /**
     * 学历:研究生及以上、本科、专科、高中/中专、初中及以下 COLUMN: education
     */
    private String education;

    /**
     * 创建时间 COLUMN: create_time
     */
    private String createTime;

    /**
     * 更新时间 COLUMN: update_time
     */
    private String updateTime;

    /**
     * 创建人ID COLUMN: create_user
     */
    private String createUser;

    /**
     * 更新人ID COLUMN: update_user
     */
    private String updateUser;

    /**
     * 创建人NAME COLUMN: create_user_name
     */
    private String createUserName;

    /**
     * 更新人NAME COLUMN: update_user_name
     */
    private String updateUserName;

    /**
     * version COLUMN: version
     */
    private Integer version;

    /**
     * 删除标记  COLUMN: is_delete
     */
    private Integer isDelete;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEmailCertified() {
        return emailCertified;
    }

    public void setEmailCertified(Integer emailCertified) {
        this.emailCertified = emailCertified;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getMobileCertified() {
        return mobileCertified;
    }

    public void setMobileCertified(Integer mobileCertified) {
        this.mobileCertified = mobileCertified;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdImgBackUrl() {
        return idImgBackUrl;
    }

    public void setIdImgBackUrl(String idImgBackUrl) {
        this.idImgBackUrl = idImgBackUrl;
    }

    public String getIdImgFrontUrl() {
        return idImgFrontUrl;
    }

    public void setIdImgFrontUrl(String idImgFrontUrl) {
        this.idImgFrontUrl = idImgFrontUrl;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Integer getIdCertified() {
        return idCertified;
    }

    public void setIdCertified(Integer idCertified) {
        this.idCertified = idCertified;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getRegProv() {
        return regProv;
    }

    public void setRegProv(String regProv) {
        this.regProv = regProv;
    }

    public String getRegCity() {
        return regCity;
    }

    public void setRegCity(String regCity) {
        this.regCity = regCity;
    }

    public String getRegCountry() {
        return regCountry;
    }

    public void setRegCountry(String regCountry) {
        this.regCountry = regCountry;
    }

    public String getRegAdd() {
        return regAdd;
    }

    public void setRegAdd(String regAdd) {
        this.regAdd = regAdd;
    }

    public String getPlaceProv() {
        return placeProv;
    }

    public void setPlaceProv(String placeProv) {
        this.placeProv = placeProv;
    }

    public String getPlaceCity() {
        return placeCity;
    }

    public void setPlaceCity(String placeCity) {
        this.placeCity = placeCity;
    }

    public String getPlaceCountry() {
        return placeCountry;
    }

    public void setPlaceCountry(String placeCountry) {
        this.placeCountry = placeCountry;
    }

    public String getPlaceAddress() {
        return placeAddress;
    }

    public void setPlaceAddress(String placeAddress) {
        this.placeAddress = placeAddress;
    }

    public String getRegProvName() {
        return regProvName;
    }

    public void setRegProvName(String regProvName) {
        this.regProvName = regProvName;
    }

    public String getRegCityName() {
        return regCityName;
    }

    public void setRegCityName(String regCityName) {
        this.regCityName = regCityName;
    }

    public String getRegCountryName() {
        return regCountryName;
    }

    public void setRegCountryName(String regCountryName) {
        this.regCountryName = regCountryName;
    }

    public String getPlaceProvName() {
        return placeProvName;
    }

    public void setPlaceProvName(String placeProvName) {
        this.placeProvName = placeProvName;
    }

    public String getPlaceCityName() {
        return placeCityName;
    }

    public void setPlaceCityName(String placeCityName) {
        this.placeCityName = placeCityName;
    }

    public String getPlaceCountryName() {
        return placeCountryName;
    }

    public void setPlaceCountryName(String placeCountryName) {
        this.placeCountryName = placeCountryName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getQqOpenId() {
        return qqOpenId;
    }

    public void setQqOpenId(String qqOpenId) {
        this.qqOpenId = qqOpenId;
    }

    public String getWeixinOpenId() {
        return weixinOpenId;
    }

    public void setWeixinOpenId(String weixinOpenId) {
        this.weixinOpenId = weixinOpenId;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Integer getIsConsole() {
        return isConsole;
    }

    public void setIsConsole(Integer isConsole) {
        this.isConsole = isConsole;
    }

    public Integer getConsoleState() {
        return consoleState;
    }

    public void setConsoleState(Integer consoleState) {
        this.consoleState = consoleState;
    }

    public String getRecommendCode() {
        return recommendCode;
    }

    public void setRecommendCode(String recommendCode) {
        this.recommendCode = recommendCode;
    }

    public String getRecommendName() {
        return recommendName;
    }

    public void setRecommendName(String recommendName) {
        this.recommendName = recommendName;
    }

    public String getRecommendDeptId() {
        return recommendDeptId;
    }

    public void setRecommendDeptId(String recommendDeptId) {
        this.recommendDeptId = recommendDeptId;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}